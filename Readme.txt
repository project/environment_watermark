CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module enable a watermark that is shown throught CSS on admin and front
pages.

REQUIREMENTS
------------

The module do not have any dependence.

INSTALLATION
------------

Enable the module with the extension list of Drupal.

CONFIGURATION
-------------

Go to /admin/config/development/environment-watermark and mark as active and
give a name to the environment.

We recommended to use the module Config Ignore or Config Split to have different
configuration per environment.

Additional you can set it active and save the text throught the settings.php
for example adding this lines:

$config['environment_watermark.settings']['active'] = TRUE;
$config['environment_watermark.settings']['watermark_text'] = 'Localhost';
