<?php

namespace Drupal\environment_watermark\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Environment watermark settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'environment_watermark_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['environment_watermark.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active watermark'),
      '#default_value' => $this->config('environment_watermark.settings')->get('active'),
    ];
    $form['watermark_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Watermark text'),
      '#default_value' => $this->config('environment_watermark.settings')->get('watermark_text'),
      '#description' => $this->t('You must clear cache after any change of these options.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('environment_watermark.settings')
      ->set('active', $form_state->getValue('active'))
      ->set('watermark_text', $form_state->getValue('watermark_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
