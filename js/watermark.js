/**
 * @file
 * Watermark functionality.
 */

(function ($, Drupal, once, drupalSettings) {

  'use strict';

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.environment_watermark = {
    attach: function (context, settings) {
      $(once('environment-watermark-init', 'body'), context).each(function () {
        $('body').append('<div id="environment-watermark">' + drupalSettings.environment_watermark.watermark_text + '</div>');
      });

    }
  };

})(jQuery, Drupal, once, drupalSettings);
